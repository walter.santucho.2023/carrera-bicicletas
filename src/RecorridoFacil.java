class RecorridoFacil extends Recorrido {
    public RecorridoFacil(String nombre, double distancia) {
        super(nombre, distancia);
    }

    @Override
    public double calcularTiempoEstimado() {
        return getDistancia() / 30;
    }

    @Override
    public double calcularPuntaje() {
        return 1;
    }
}