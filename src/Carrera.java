import java.util.ArrayList;
import java.util.List;

public class Carrera {
    List<Ciclista> ciclistas = new ArrayList<Ciclista>();

    public void agregarCiclista(Ciclista unCiclista)
    {
        ciclistas.add(unCiclista);
    }

    public List<Ciclista> ciclistasConPuntajeMayorA30()
    {
        List<Ciclista> resultado = new ArrayList<Ciclista>();

        for(Ciclista ciclistaActual: ciclistas)
        {
            if (ciclistaActual.calcularPuntaje() > 30)
            {
                resultado.add(ciclistaActual);
            }
        }

        return resultado;
    }

    public Ciclista ciclistaMayorTiempoEstimado()
    {
        Ciclista ciclistaMayorTiempo = ciclistas.get(0);

        for(Ciclista ciclistaActual: ciclistas)
        {
            if (ciclistaActual.calcularTiempoEstimado() > ciclistaMayorTiempo.calcularTiempoEstimado())
            {
                ciclistaMayorTiempo = ciclistaActual;
            }
        }

        return ciclistaMayorTiempo;
    }

    public List<Ciclista> ciclistasQueTerminaranEnMenosDe2Horas()
    {
        List<Ciclista> resultado = new ArrayList<Ciclista>();

        for(Ciclista ciclistaActual: ciclistas)
        {
            if (ciclistaActual.terminaraEnMenosDe2Horas())
            {
                resultado.add(ciclistaActual);
            }
        }

        return resultado;
    }
}
