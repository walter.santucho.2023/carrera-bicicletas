import java.util.ArrayList;

class Ciclista {
    private String nombre;
    private double velocidadPromedio;
    private ArrayList<Recorrido> recorridos = new ArrayList<>();

    public Ciclista(String nombre, double velocidadPromedio) {
        this.nombre = nombre;
        this.velocidadPromedio = velocidadPromedio;
    }

    public void agregarRecorrido(Recorrido recorrido) {
        recorridos.add(recorrido);
    }

    public double calcularTiempoEstimado() {
        double tiempoEstimadoTotal = 0.0;
        for (Recorrido recorrido : recorridos) {
            tiempoEstimadoTotal += recorrido.calcularTiempoEstimado();
        }
        return tiempoEstimadoTotal;
    }

    public double calcularPuntaje() {
        double puntajeTotal = 0.0;
        for (Recorrido recorrido : recorridos) {
            puntajeTotal += recorrido.calcularPuntaje();
        }
        return puntajeTotal;
    }

    public boolean terminaraEnMenosDe2Horas() {
        double distanciaTotal = 0.0;
        for (Recorrido recorrido : recorridos) {
            distanciaTotal += recorrido.getDistancia();
        }
        double tiempoEstimado = distanciaTotal / velocidadPromedio;
        return tiempoEstimado < 2.0;
    }
}