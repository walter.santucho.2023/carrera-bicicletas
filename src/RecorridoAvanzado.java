class RecorridoAvanzado extends Recorrido {
    private int obstaculos;

    public RecorridoAvanzado(String nombre, double distancia, int obstaculos) {
        super(nombre, distancia);
        this.obstaculos = obstaculos;
    }

    @Override
    public double calcularTiempoEstimado() {
        return getDistancia() / (20 * obstaculos);
    }

    @Override
    public double calcularPuntaje() {
        return 0.5 * getDistancia();
    }
}
