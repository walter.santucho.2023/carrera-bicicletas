abstract class Recorrido {
    private String nombre;
    private double distancia;

    public Recorrido(String nombre, double distancia) {
        this.nombre = nombre;
        this.distancia = distancia;
    }

    abstract public double calcularTiempoEstimado();

    abstract public double calcularPuntaje();

    public double getDistancia() {
        return distancia;
    }
}