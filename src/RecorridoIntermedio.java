class RecorridoIntermedio extends Recorrido {
    private boolean pendienteAscendente;

    public RecorridoIntermedio(String nombre, double distancia, boolean pendienteAscendente) {
        super(nombre, distancia);
        this.pendienteAscendente = pendienteAscendente;
    }

    @Override
    public double calcularTiempoEstimado() {
        double indice = pendienteAscendente ? 1.5 : 0.5;
        return getDistancia() / (25 * indice);
    }

    @Override
    public double calcularPuntaje() {
        return 1 + (pendienteAscendente ? 1.5 : 0.5);
    }
}
